// 1. In this file we are check is name, email, password empty or not
// 2. if email is exist already then shows the msg has user already exists.

const express = require('express');
const router = express.Router();

const gravtar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');

const { check, validationResult} = require('express-validator');

const User = require('../../models/User');

// route Post  api/users
// desc register user
// access Public

router.post('/',[
    check ('name','Name is required')
    .not()
    .isEmpty(),
    check('email','please include a valid email').isEmail(),
    check('password','please enter a password with 6 or more characters'
    ).isLength({min:6})
    
],
async (req,res)=> {
    const errors = validationResult (req);
    if (!errors.isEmpty()){
        return res.status(400).json({ errors :errors.array()});

    }
    
    const { name, email, password } = req.body;

    try{
        // see if the user exist
        let user = await User.findOne({ email});

        if (user){
            return res.status(400).json({ errors :[{ msg: 'User already exists'}] });
        }

        // Get user gravtar(image)

        const avatar = gravtar.url(email,{
            s:'200',
            r:'pg',
            d:'mm'
        })

        // if new user login then name email avatar password stored in the database

        user = new User({
            name,
            email,
            avatar,
            password
        });

        // encrypt password- which password are enter by user it will be in secure mode ie. bcrypted

        const salt = await bcrypt.genSalt(10);

        user.password = await bcrypt.hash(password,salt);
        await user.save();

        const payload = {
            user :{
                id:user.id
            }
        }

        // return jsonwebtoken - JWT, or JSON Web Token, is an open standard used to share security information between two parties — a client and a server.

        jwt.sign(
            payload,
            config.get('jwtSecret'),
            {expiresIn:36000},
            (err,token) => {
                if (err) throw err;
                res.json({token});
            }
            
            );

        
        // res.send('User registered');
        
    }catch(err){
        console.error(err.message);
        res.status(500).send('Server error')

    }

   


    // console.log(req.body);
    
});

module.exports = router;